import logging

import allure
import pytest

from Login_Page import SearchHelper


@pytest.mark.smoke_test_modptk
@allure.feature('Smoke тестирование на Chrome. Авторизация.')
@allure.story('Успешная авторизация')
@allure.severity('Critical')
def test_admin_login_successfull_chrome(browser_chrome_bs):
        logger = logging.getLogger('modPTK_logger')
        logger.setLevel(logging.DEBUG)
        # create file handler which logs even debug messages
        fh = logging.FileHandler('admin_login_successfull.txt')
        fh.setLevel(logging.DEBUG)
        # create console handler with a higher log level
        ch = logging.StreamHandler()
        ch.setLevel(logging.DEBUG)
        consoleHandler = logging.StreamHandler()
        consoleHandler.setLevel(logging.DEBUG)
        # create formatter and add it to the handlers
        formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
        ch.setFormatter(formatter)
        fh.setFormatter(formatter)
        # add the handlers to logger
        logger.addHandler(ch)
        logger.addHandler(fh)
        ptk_login = SearchHelper(browser_chrome_bs)
        ptk_login.go_to_login()
        ptk_login.authorization_assert()

