import logging

import pytest
from selenium import webdriver
from selenium.webdriver.firefox.options import Options


@pytest.fixture()
def browser_chrome_bs():
    driver = webdriver.Remote(
            options=webdriver.ChromeOptions(),
            command_executor='http://selenium__standalone-chrome:4444/wd/hub'
        )
    return driver
