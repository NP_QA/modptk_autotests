import itertools
import logging
import random
import time

import allure
from allure_commons.types import AttachmentType
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys

from BaseApp import BasePage


class PageLocators:
    locator_ptk_go_to_login = 'https://dev.modptk.unc-esk.ru/login'
    locator_ptk_login_without_user = 'https://dev.modptk.unc-esk.ru/ipr'
    locator_ptk_fill_login = (By.XPATH, '321')
    locator_ptk_fill_password = (By.XPATH, '//div[3]/div/div/input')
    locator_ptk_authorization_assert = (By.XPATH, '//span[contains(text(), "Корректировка ИПР")]')
    locator_ptk_login_button = (By.XPATH, '//span[@class="MuiButton-label" and contains(text(), "Войти")]')
    locator_ptk_find_project_by_3_last_numbers = (By.XPATH, '//*[@id="root"]/div/div[2]/div[2]/a[1]/div[1]/div[1]/p')
    locator_ptk_searching_field = (By.XPATH, '//*[@placeholder="Поиск..."]')
    locator_ptk_project_edit_icon = (By.XPATH, '//span[@class="edit-icon"]')
    locator_ptk_get_task_name = (By.XPATH, '//*[@id="root"]/div[1]/div[2]/div[2]/a[1]/div[1]/div[2]/p')
    locator_ptk_get_project_filial = (By.XPATH, '//*[@id="root"]/div[1]/div[2]/div[2]/a[1]/div[1]/div[3]/p')
    locator_ptk_list_of_all_projects = (By.XPATH, '//*[@id="root"]/div/div[2]/div[2]')
    locator_ptk_settings_button_main_page = (By.XPATH, '//span[contains(text(), "Параметры")]')
    locator_ptk_table_invesment_programms = (By.XPATH, '//div[@class="ParamsModal_params__2rCMQ"]')
    locator_ptk_investment_parameters = (By.XPATH,
                                         '//div[@class="title ParamsModal_paramsTitle__tq70L" and contains(text(), "Параметры инвестиционной программы")]')
    locator_pkt_investment_index_def = (By.XPATH, '//div[@class="MuiGrid-root MuiGrid-container MuiGrid-spacing-xs-2"]')
    locator_ptk_click_version_selector = (By.XPATH, '//button//span//span[@class="dropdown-icon"]')
    locator_ptk_select_SD_version = (
        By.XPATH, '//*[@class="Versioning_versioning__item-name__1qiCu" and contains(text(), "Версия для СД")]')
    locator_ptk_assert_SD_version_selected = (By.XPATH, '//*[contains(text(), "Версия для СД")]')
    locator_ptk_body_of_main_page = (By.XPATH, '/html/body/div[2]/div[1]')
    locator_ptk_assert_project_id_in_card = (By.XPATH, '//*[@id="root"]/div[1]/div[1]/div/div[1]/div')
    locator_ptk_skeleton_of_main_page = (By.XPATH, '/html/body')
    locator_ptk_click_on_page_upper = (By.XPATH, '//div[@class="btn-up-icon"]')
    locator_ptk_project_card_show_button = (By.XPATH, '//button[contains(text(), "Показать полностью")]')
    locator_ptk = 'https://dev.modptk.unc-esk.ru/api/v1/program/excel'
    locator_ptk_project_description_text = (By.XPATH,
                                            '//*[contains(text(), "Тестовое описание кнопки показать полностью. ")]')
    locator_ptk_project_description_text_part_2 = (By.XPATH,
                                                   '//*[contains(text(), "Тестовое описание кнопки показать полностью. Тестовое описание кнопки показать полностью. Тестовое описание кнопки показать полностью. Тестовое описание кнопки показать полностью. Тестовое описание кнопки показать полностью. Тестовое описание кнопки показать полностью. Тестовое описание кнопки показать полностью. Тестовое описание кнопки показать полностью. Тестовое описание кнопки показать полностью.")]')
    locator_ptk_go_to_test_project = 'https://dev.modptk.unc-esk.ru/correction-ipr/project/2060'
    locator_ptk_go_to_personal_project = 'https://dev.modptk.unc-esk.ru/personal-projects/project/28'
    locator_ptk_go_to_personal_project_2 = 'https://dev.modptk.unc-esk.ru/personal-projects/project/52'
    locator_ptk_first_click_download_xlsx_project_card = (
        By.XPATH, '//span[@class="MuiButton-label" and contains(text(), "Выгрузить")]')
    locator_ptk_click_download_xlsx_project_card = (By.XPATH, '//button[2]//span[contains(text(), "Выгрузить")]')
    locator_ptk_project_card_select_region = (By.XPATH, '//input[@class="MuiInputBase-input MuiFilledInput-input"]')
    locator_ptk_project_card_select_altay_region = (By.XPATH, '//ul//div//li[1][contains(text(), "Алтайский край")]')
    locator_ptk_project_card_select_amur_region = (By.XPATH, '//ul//div//li[2][contains(text(), "Амурская обл.")]')
    locator_ptk_project_card_check_altay_selected = (
        By.XPATH, '//*[contains(text(), "Алтайский край;")]')
    locator_ptk_project_card_check_amur_selected = (
        By.XPATH, '//*[contains(text(), "Амурская обл.;")]')
    locator_ptk_project_card_click_on_body = (By.XPATH, '/html/body')
    locator_ptk_project_card_commentary = (
        By.XPATH, '//div[3]//div[2]//div/div//input[@class="MuiInputBase-input MuiFilledInput-input"]')
    locator_ptk_comment = (By.XPATH, '//*[@id="root"]/div[1]/div[2]/div[3]/div[2]/div/div/input')
    locator_ptk_project_card_click_on_calculation = (
        By.XPATH, '//span[@class="MuiButton-label" and contains(text(), "Рассчитать")]')
    locator_ptk_project_card_calculation_unc = (
        By.XPATH,
        '//span[@class="SubInfoBlock_infoBlockTitle__1c1CT" and contains(text(), "Сумма расценок УНЦ без НДС")]')
    locator_ptk_project_card_calculation_unc_i_nds = (By.XPATH,
                                                      '//*[contains(text(), "Сумма ненормируемых затрат с НДС")]')
    locator_ptk_project_card_calculation_unc_prognoz = (By.XPATH,
                                                        '//span[@class="SubInfoBlock_infoBlockTitle__1c1CT" and contains(text(), "16.4. Стоимость по УНЦ в прогнозных ценах")]')
    locator_ptk_project_card_calculation_block_1 = (By.XPATH, '//div[@class="table__head table__head-fixed"]')
    locator_ptk_project_card_calculation_title = (
        By.XPATH, '//div[@class="title" and contains(text(), "Расчёт стоимости по УНЦ")]')
    locator_ptk_project_card_cancel_calculation = (By.XPATH, '//span[@class="close-icon"]')
    locator_ptk_project_card_summa_s_nds = (
        By.XPATH, '//div[2]/div[1]/div[2]/div/div//input[@class="MuiInputBase-input MuiFilledInput-input"]')
    locator_ptk_assert_summa_s_nds = (By.XPATH, '//*[@id="root"]/div[1]/div[3]/div[2]/div[1]/div[2]/span[2]')
    locator_ptk_project_card_edited_summa_s_nds = (
        By.XPATH, '//*[@id="root"]/div[1]/div[3]/div[2]/div[1]/div[2]/div[1]/div/input')
    locator_ptk_project_card_click_on_plus = (By.XPATH, '//*[@class="plus-icon"]')
    locator_ptk_project_card_select_table_1 = (By.CSS_SELECTOR, 'div.unc-pricing-tables__list > div:nth-child(1) > div')
    locator_ptk_project_card_select_b_1 = (By.CSS_SELECTOR,
                                           'div.MuiCollapse-container.unc-pricing-item__detail.MuiCollapse-entered > div > div > div > div:nth-child(1) > div')
    locator_ptk_project_card_select_product = (By.XPATH, '//table/tbody/tr[1]/td[4]')
    locator_ptk_project_card_fill_amount = (
        By.XPATH, '//div[3]//div//div[2]//div[1]//div[2]//div[1]//div//input')
    locator_ptk_project_card_assert_value_amount = (
        By.XPATH, '//*[@id="root"]/div[1]/div[3]/div[3]/div/div/div/div[1]/div[2]/div[2]/div[4]/div/div/input')
    locator_ptk_project_card_save_button = (
        By.XPATH, '//*[@id="root"]/div[1]/div[1]/div/div[2]/div/div[2]/button/span[1]')
    locator_ptk_project_card_save_unc_button = (
        By.XPATH, '/html/body/div[2]/div[3]/div/div[3]/div/div[3]/button[2]/span[1]')
    locator_ptk_project_card_pick_table_2 = (By.XPATH, '/html/body/div[2]/div[3]/div[3]/div/div[2]/div[2]/div[1]/span')
    locator_ptk_project_card_pick_table_3 = (
        By.XPATH, '/html/body/div[2]/div[3]/div/div[3]/div/div[2]/div[3]/div[1]/span')
    locator_ptk_project_card_fill_amount_table_2 = (
        By.CSS_SELECTOR,
        'div.MuiFormControl-root.MuiTextField-root.jss5.\'.changing-unc-table-data__body-count\' > div > input')
    locator_ptk_project_card_fill_amount_table_3 = (
        By.XPATH, '/html/body/div[2]/div[3]/div/div[3]/div/div[2]/div[3]/div[2]/div[1]/div/input')
    locator_ptk_project_card_assert_value_amount_2 = (By.XPATH, '//div[2]/div[4]/div[4]/div/div/input')
    locator_ptk_calculation_test_card = 'https://dev.modptk.unc-esk.ru/correction-ipr/project/3843'
    locator_ptk_project_card_delete = (
        By.XPATH, '//*[@id="root"]/div[1]/div[3]/div[3]/div/div/div/div[1]/div[2]/div[2]/div[12]/button')
    locator_ptk_project_card_delete_2 = (
        By.XPATH, '//*[@id="root"]/div[1]/div[3]/div[3]/div/div/div/div[1]/div[2]/div[4]/div[12]/button')
    locator_ptk_project_card_assert_value_amount_3 = (
        By.XPATH, '//*[@id="root"]/div[1]/div[3]/div[3]/div/div/div/div[1]/div[2]/div[6]/div[4]/div/div/input')
    locator_ptk_project_card_delete_3 = (
        By.XPATH, '//*[@id="root"]/div[1]/div[3]/div[3]/div/div/div/div[1]/div[2]/div[6]/div[12]/button')
    locator_ptk_project_card_pick_table_4 = (
        By.XPATH, '/html/body/div[2]/div[3]/div/div[3]/div/div[2]/div[4]/div[1]/span')
    locator_ptk_project_card_fill_amount_4 = (
        By.XPATH, '/html/body/div[2]/div[3]/div/div[3]/div/div[2]/div[4]/div[2]/div[1]/div/input')
    locator_ptk_project_card_assert_value_amount_4 = (
        By.XPATH, '//*[@id="root"]/div[1]/div[3]/div[3]/div/div/div/div[1]/div[2]/div[8]/div[4]/div/div/input')
    locator_ptk_project_card_delete_4 = (
        By.XPATH, '//*[@id="root"]/div[1]/div[3]/div[3]/div/div/div/div[1]/div[2]/div[8]/div[12]/button')
    locator_ptk_project_card_pick_table_5 = (
        By.XPATH, '/html/body/div[2]/div[3]/div/div[3]/div/div[2]/div[5]/div[1]/span')
    locator_ptk_project_card_fill_amount_5 = (
        By.XPATH, '/html/body/div[2]/div[3]/div/div[3]/div/div[2]/div[5]/div[2]/div[1]/div/input')
    locator_ptk_project_card_assert_value_amount_5 = (
        By.XPATH, '//*[@id="root"]/div[1]/div[3]/div[3]/div/div/div/div[1]/div[2]/div[10]/div[4]/div/div/input')
    locator_ptk_project_card_delete_5 = (
        By.XPATH, '//*[@class="delete-icon"]')
    locator_ptk_project_card_assert_deleted = (
        By.XPATH, '//div[10][@class="table__body-row table__body-nothing" and contains(text(), "Отсутствует")]')
    locator_ptk_project_card_click_instructions = (By.XPATH, '//div[2]//div[3]//div[2]//div[1]//button')
    locator_ptk_project_card_find_istructions = (By.XPATH,
                                                 '//div[@class="selected-unc-table__instructions visible" and contains(text(), "К таблицам В1, В2. В УНЦ ячейки выключателя НУ 6-750 кВ включено: стоимость оборудования (выключатель (ячейка с выключателем, КРУН) или альтернативное решение (компактный модуль), элементы управления ячейкой, разъединители, трансформаторы тока (далее – ТТ) (в том числе цифровые ТТ), трансформаторы напряжения  (далее – ТН) (в том числе цифровые ТН), ограничители перенапряжений (далее – ОПН), шкафы РЗА, автоматика управления выключателем, блоки управления приводами разъединителей, шкафы наружной установки (обогрева выключателя, питания приводов разъединителей, обогрева приводов разъединителей, зажимов выключателя), приборы учета и измерения электроэнергии КРУН), стоимость строительно-монтажных работ (в том числе демонтаж существующего оборудования) с учетом стоимости используемого материала (устройство фундаментов, опорных стоек и металлоконструкций, порталов, ошиновки, кабельного хозяйства,заземления), а также сопутствующие затраты. К таблицам В1-В5. При отличных от представленных максимальных (минимальных) характеристик типовых технологических решений (номинальный ток, номинальный ток отключения)УНЦ принимается равным максимальному (минимальному) значению УНЦ соответствующего напряжения технологического решения, указанного в таблице.")]')
    locator_ptk_project_card_add_comment = (By.XPATH, '//input[@class="MuiInputBase-input MuiInput-input"]')
    locator_ptk_three_dots_icon = (By.XPATH, '//span[@class="three-dots-icon"]')
    locator_ptk_click_archive_form_20 = (By.XPATH, '//span[contains(text(), "Архив форм 20")]')
    locator_ptk_archive_form_20_searching_by_id = (By.XPATH, '//input[@class="IdsInserter_inserterInput__37LaO"]')
    locator_ptk_download_button = (By.XPATH, '//span[@class="MuiButton-label" and contains(text(), "Выгрузить")]')
    locator_ptk_click_excel_form = (By.XPATH, '//span[contains(text(), "Excel")]')
    locator_ptk_edit_version = (By.XPATH, '//*[@class="Versioning_versioning__item-select__1l4_h select-version-icon"]')
    locator_ptk_version_hover = (By.XPATH, '/html/body/div[2]/div[3]/div/div/div[2]/div[1]')
    locator_ptk_click_on_rename_button = (By.XPATH, '//*[contains(text(), "Переименовать")]')
    locator_ptk_hover_renamed = (By.XPATH, '/html/body/div[2]/div[3]/div/div/div[2]/div[1]/li/div[1]')
    locator_ptk_input_rename = (By.XPATH, '/html/body/div[2]/div[3]/div/div/div[2]/div[1]/li/div[1]/div/input')
    locator_ptk_find_rename_alert = (By.XPATH, '//*[@role="alert"]')
    locator_ptk_create_new_version = (By.XPATH, '//*[contains(text(), "Создать новую версию")]')
    locator_ptk_new_version_name = (By.XPATH, '/html/body/div[2]/div[3]/div/div/div[2]/div[1]/li/div[1]/div/input')
    locator_ptk_back_to_main_page_arrow = (By.XPATH, '//span[@class="arrow-left-icon"]')
    locator_ptk_select_non_actual_version = (
        By.CSS_SELECTOR, 'div.Versioning_versioning__list__20L62 > div:nth-child(2) > li')
    locator_ptk_region_disabled = (By.XPATH, '//*[@id="root"]/div[1]/div[2]/div[3]/div[1]/div/div/input[@disabled]')
    locator_ptk_comment_disabled = (By.XPATH, '//*[@id="root"]/div[1]/div[2]/div[3]/div[2]/div/div/input[@disabled]')
    locator_ptk_nds_disabled = (
        By.XPATH, '//*[@id="root"]/div[1]/div[3]/div[2]/div[1]/div[2]/div[1]/div/input[@disabled]')
    locator_ptk_click_rasschet_button = (By.XPATH, '//*[contains(text(), "Рассчитать")]')
    locator_ptk_select_actual_version = (By.XPATH, '//*[contains(text(), "Текущая версия")]')
    locator_ptk_project_card_info_block = (By.XPATH, '//*[@class="info-block__value"]')
    locator_ptk_project_card_click_region_selector = (By.XPATH, '//div[5]//div//div')
    locator_ptk_project_card_click_region_selector_after_save = (By.XPATH, '//div[4]//div//div//input')
    locator_ptk_project_card_comment_icon = (By.XPATH, '//*[@class="comment-icon"]')
    locator_ptk_project_card_edit_comment = (By.XPATH, '//*[contains(text(), "Редактировать")]')
    locator_ptk_project_card_comment_textarea = (By.XPATH, '//div[2]/div[2]/div/textarea')
    locator_ptk_project_card_save_comment_button = (By.XPATH, '//*[contains(text(), "Сохранить")]')
    locator_ptk_project_card_verify_project = (By.XPATH, '//*[contains(text(), "Проверить проект")]')
    locator_ptk_project_card_click_verify_project = (
        By.XPATH, '//span[@class="MuiButton-label" and contains(text(), "Проверить")]')
    locator_ptk_project_card_preparation_of_the_territory = (By.XPATH, '//div/label[6]/span[1]')
    locator_ptk_project_card_bifurcations_with_cells = (By.XPATH, '//div/label[5]/span[1]')
    locator_ptk_project_card_other_burification = (By.XPATH, '//div/label[4]/span[1]')
    locator_ptk_project_card_verify_table = (
        By.XPATH, '//div[3]/div/div/div[2]/div[1]/span[2][contains(text(), "Ошибки:")]')
    locator_ptk_project_card_comment_calculation = (By.XPATH, '//div[2]/div[10]/div/div/input')


class SearchHelper(BasePage):

    def login(self):
        assert self.driver.get(PageLocators.locator_ptk_go_to_login)

    def go_to_login(self):
        try:
            self.driver.get(PageLocators.locator_ptk_go_to_login)
        except:
            exit(1)

    def comment_calculation(self):
        try:
            assert self.find_element(PageLocators.locator_ptk_project_card_comment_calculation).send_keys(
                'Проверка комментирования рассчетов')
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Комментарий в поле комментирования рассчетов оставлен. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Комментарий в поле комментирования рассчетов оставлен. Успешный.',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Комментарий в поле комментирования рассчетов оставлен. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Комментарий в поле комментирования рассчетов оставлен. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def assert_project_card_verified(self):
        try:
            assert self.find_element(PageLocators.locator_ptk_project_card_verify_table)
            find_bugs = self.find_element(PageLocators.locator_ptk_project_card_verify_table)
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Проверка, что проект проверен. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Проверка, что проект проверен. Успешный.',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Проверка, что проект проверен. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Проверка, что проект проверен. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def click_bifurcations_with_cells(self):
        try:
            assert self.find_clickable_element(PageLocators.locator_ptk_project_card_bifurcations_with_cells)
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Клик на чек-бокс Задвоения с ячейками. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Клик на чек-бокс Задвоения с ячейками. Успешный.',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Клик на чек-бокс Задвоения с ячейками. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Клик на чек-бокс Задвоения с ячейками. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def click_other_burifications(self):
        try:
            assert self.find_clickable_element(PageLocators.locator_ptk_project_card_other_burification)
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Клик на чек-бокс Задвоения прочие. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Клик на чек-бокс Задвоения прочие. Успешный.',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Клик на чек-бокс Задвоения прочие. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Клик на чек-бокс Задвоения прочие. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def click_preparation_of_the_territory(self):
        try:
            assert self.find_clickable_element(PageLocators.locator_ptk_project_card_preparation_of_the_territory)
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Клик на чек-бокс Подготовка территории. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Клик на чек-бокс Подготовка территории. Успешный.',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Клик на чек-бокс Подготовка территории. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Клик на чек-бокс Подготовка территории. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def project_card_click_comment_icon(self):
        try:
            assert self.find_clickable_element(PageLocators.locator_ptk_project_card_comment_icon)
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Клик на иконку комментария. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Клик на иконку комментария. Успешный.',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Клик на иконку комментария. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Клик на иконку комментария. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def project_card_click_verify_project(self):
        try:
            assert self.find_clickable_element(PageLocators.locator_ptk_project_card_verify_project)
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Клик на кнопку "Проверить проект". Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Клик на кнопку "Проверить проект". Успешный.',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Клик на кнопку "Проверить проект". Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Клик на кнопку "Проверить проект". Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def project_card_click_verify_project_button(self):
        try:
            assert self.find_clickable_element(PageLocators.locator_ptk_project_card_click_verify_project)
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Клик на кнопку "Проверить проект". Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Клик на кнопку "Проверить проект". Успешный.',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Клик на кнопку "Проверить проект". Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Клик на кнопку "Проверить проект". Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def project_card_click_edit_comment(self):
        try:
            assert self.find_clickable_element(PageLocators.locator_ptk_project_card_edit_comment)
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Клик на иконку комментария. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Клик на иконку комментария. Успешный.',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Клик на иконку комментария. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Клик на иконку комментария. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def project_card_save_comment_button(self):
        try:
            assert self.find_clickable_element(PageLocators.locator_ptk_project_card_save_comment_button)
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Сохранил комментарий. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Сохранил комментарий. Успешный.',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Сохранил комментарий. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Сохранил комментарий. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def project_card_fill_comment_textarea(self):
        try:
            assert self.find_element(PageLocators.locator_ptk_project_card_comment_textarea).send_keys('QA проверка')
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Клик на иконку комментария. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Клик на иконку комментария. Успешный.',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Клик на иконку комментария. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Клик на иконку комментария. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def project_card_clear_comment_textarea(self):
        try:
            assert self.find_element(PageLocators.locator_ptk_project_card_comment_textarea).send_keys(
                Keys.CONTROL + 'A',
                Keys.DELETE)
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Клик на иконку комментария. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Клик на иконку комментария. Успешный.',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Клик на иконку комментария. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Клик на иконку комментария. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def project_card_find_comment_text(self):
        try:
            assert self.find_element(PageLocators.locator_ptk_project_card_comment_textarea).text == 'QA проверка'
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Клик на иконку комментария. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Клик на иконку комментария. Успешный.',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Клик на иконку комментария. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Клик на иконку комментария. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def project_card_close_comment_textarea(self):
        try:
            assert self.find_clickable_element(PageLocators.locator_ptk_project_card_cancel_calculation)
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Клик на иконку крестика, для закрытия комментария. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Клик на иконку крестика, для закрытия комментария. Успешный.',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Клик на иконку крестика, для закрытия комментария. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Клик на иконку крестика, для закрытия комментария. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def go_to_another_personal_project(self):
        try:
            assert self.driver.get(PageLocators.locator_ptk_go_to_personal_project_2)
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Переход на страницу логина. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Переход на страницу логина. Успешный.',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Переход на страницу логина. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Переход на страницу логина. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def save_unc_button(self):
        try:
            assert self.find_element(
                PageLocators.locator_ptk_project_card_save_unc_button)
            save_button_unc = self.find_element(
                PageLocators.locator_ptk_project_card_save_unc_button)
            self.driver.execute_script("arguments[0].click()", save_button_unc)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Клик на кнопку "Сохранить" в таблице расценок УНЦ. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Клик на кнопку "Сохранить" в таблице расценок УНЦ. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def project_edit_icon(self):
        try:
            assert self.find_clickable_element(PageLocators.locator_ptk_project_edit_icon)
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Клик на кнопку редактирования проекта. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Клик на кнопку редактирования проекта. Успешный.',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Клик на кнопку редактирования проекта. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Клик на кнопку редактирования проекта. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def project_click_region_selector(self):
        try:
            assert self.find_clickable_element(PageLocators.locator_ptk_project_card_click_region_selector)
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Клик на кнопку выбора региона проекта. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Клик на кнопку выбора региона проекта. Успешный.',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Клик на кнопку выбора региона проекта. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Клик на кнопку выбора региона проекта. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def project_click_region_selector_after_save(self):
        try:
            assert self.find_clickable_element(PageLocators.locator_ptk_project_card_click_region_selector_after_save)
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Клик на кнопку выбора региона проекта после сохранения. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Клик на кнопку выбора региона проекта после сохранения. Успешный.',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Клик на кнопку выбора региона проекта после сохранения. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Клик на кнопку выбора региона проекта после сохранения. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def go_to_personal_project(self):
        try:
            assert self.driver.get(PageLocators.locator_ptk_go_to_personal_project)
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Переход на страницу личного проекта. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Переход на страницу личного проекта. Успешный.',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Переход на страницу личного проекта. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Переход на страницу личного проекта. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def go_to_test_project(self):
        try:
            assert self.driver.get(PageLocators.locator_ptk_go_to_test_project)
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Переход на страницу тестового проект. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Переход на страницу тестового проект. Успешный.',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Переход на страницу личного проекта. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Переход на страницу личного проекта. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def go_to_login_without_user(self):
        try:
            assert self.driver.get(PageLocators.locator_ptk_login_without_user)
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Переход на страницу логина будучи незалогиненным. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Переход на страницу логина будучи незалогиненным. Успешный.',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Переход на страницу логина будучи незалогиненным. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Переход на страницу логина будучи незалогиненным. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def select_actual_version(self):
        try:
            assert self.find_clickable_element(PageLocators.locator_ptk_select_actual_version)
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Выбор актуальной версии проекта. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Выбор актуальной версии проекта. Успешный.',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Выбор актуальной версии проекта. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Выбор актуальной версии проекта. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def click_rasschet_button(self):
        try:
            assert self.find_clickable_element(PageLocators.locator_ptk_select_actual_version)
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Выбор актуальной версии проекта. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Выбор актуальной версии проекта. Успешный.',
                              attachment_type=AttachmentType.PNG)
            find_rasschet_button = self.find_element(PageLocators.locator_ptk_click_rasschet_button)
            assert self.find_element(PageLocators.locator_ptk_click_rasschet_button)
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Клик на кнопку рассчета. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Клик на кнопку рассчета. Успешный.',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Клик на кнопку рассчета. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Клик на кнопку рассчета. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def assert_nds_disabled(self):
        try:
            assert self.find_element(PageLocators.locator_ptk_nds_disabled)
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Проверка, что кнопка НДС неактивна. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Проверка, что кнопка НДС неактивна. Успешный.',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Проверка, что кнопка НДС неактивна. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Проверка, что кнопка НДС неактивна. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def click_three_dots_icon(self):
        try:
            assert self.find_clickable_element(PageLocators.locator_ptk_three_dots_icon)
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Клик на кнопку с троеточием. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Клик на кнопку с троеточием. Успешный.',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Клик на кнопку с троеточием. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Клик на кнопку с троеточием. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def back_to_main_page(self):
        try:
            assert self.find_element(PageLocators.locator_ptk_back_to_main_page_arrow)
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Поиск кнопки возвращения к главной странице. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Поиск кнопки возвращения к главной странице. Успешный.',
                              attachment_type=AttachmentType.PNG)
            back_to_main_page = self.find_element(PageLocators.locator_ptk_back_to_main_page_arrow)
            self.driver.execute_script("arguments[0].click()", back_to_main_page)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Клик на кнопку возвращения к главной странице. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Клик на кнопку возвращения к главной странице. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def assert_comment_disabled(self):
        try:
            assert self.find_element(PageLocators.locator_ptk_comment_disabled)
            self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Проверка того, что комментарий отключен. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Проверка того, что комментарий отключен. Успешный.',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Проверка того, что комментарий отключен. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Проверка того, что комментарий отключен. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def select_non_actual_version(self):
        try:
            assert self.find_clickable_element(PageLocators.locator_ptk_select_non_actual_version)
            self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Выбор неактуальной версии проекта. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Выбор неактуальной версии проекта. Успешный.',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Выбор неактуальной версии проекта. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Выбор неактуальной версии проекта. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def assert_region_disabled(self):
        try:
            assert self.find_element(PageLocators.locator_ptk_region_disabled)
            self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Проверка того, что селектор региона неактивен. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Проверка того, что селектор региона неактивен. Успешный.',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Проверка того, что селектор региона неактивен. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Проверка того, что селектор региона неактивен. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def go_to_rename_version(self):
        try:
            assert self.find_element(PageLocators.locator_ptk_version_hover)
            select_version_hover = self.find_element(PageLocators.locator_ptk_version_hover)
            hover = ActionChains(self.driver).move_to_element(select_version_hover)
            hover.perform()
            assert self.find_clickable_element(PageLocators.locator_ptk_edit_version)
            self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Переход к переименованию версии. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Переход к переименованию версии. Успешный.',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Переход к переименованию версии. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Переход к переименованию версии. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def click_on_rename_button(self):
        try:
            assert self.find_clickable_element(PageLocators.locator_ptk_click_on_rename_button)
            self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Клик на кнопку переименования версии. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Клик на кнопку переименования версии. Успешный.',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Клик на кнопку переименования версии. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Screenshot',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def send_keys_to_rename(self):
        try:
            letters_names = (
                "а", "б", "в", "г", "д", "ж", "з", "е", "и", "к", "л", "ы", "н", "к", "о", "щ", "х", "ъ", "й", "ц", "у",
                "ф",
                "ч",
                "с", "м", "т", "ь", "д")
            all_combos_names = list(itertools.combinations(letters_names, 7))
            all_combos_names = [''.join(combo) for combo in all_combos_names]
            project_name = random.sample(all_combos_names, 1)[0]
            clear_field = self.find_element(PageLocators.locator_ptk_input_rename).send_keys(Keys.CONTROL + 'A',
                                                                                             Keys.DELETE)
            send_keys = self.find_element(PageLocators.locator_ptk_input_rename).send_keys(project_name + 'Rename_test')
            click_on_body_of_main_page = self.find_clickable_element(PageLocators.locator_ptk_body_of_main_page)
            assert self.find_element(PageLocators.locator_ptk_find_rename_alert).text == send_keys
            self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Переименование версии. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Переименование версии. Успешный.',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Переименование версии. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Переименование версии. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def name_new_version(self):
        try:
            letters_names = (
                "а", "б", "в", "г", "д", "ж", "з", "е", "и", "к", "л", "ы", "н", "к", "о", "щ", "х", "ъ", "й", "ц", "у",
                "ф",
                "ч",
                "с", "м", "т", "ь", "д")
            all_combos_names = list(itertools.combinations(letters_names, 7))
            all_combos_names = [''.join(combo) for combo in all_combos_names]
            version_name = random.sample(all_combos_names, 1)[0]
            name_new_version = self.find_element(PageLocators.locator_ptk_new_version_name).send_keys(
                version_name + 'QA TEST')
            click_on_body_of_main_page = self.find_clickable_element(PageLocators.locator_ptk_body_of_main_page)
            assert self.find_element(PageLocators.locator_ptk_find_rename_alert).text == name_new_version
            self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Название новой версии. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Название новой версии. Успешный.',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Название новой версии. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Название новой версии. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def click_archive_form_20(self):
        try:
            assert self.find_clickable_element(PageLocators.locator_ptk_click_archive_form_20)
            self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Клик на кнопку выгрузки "Архив форм 20". Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Клик на кнопку выгрузки "Архив форм 20". Успешный.',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Клик на кнопку выгрузки "Архив форм 20". Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Клик на кнопку выгрузки "Архив форм 20". Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def click_create_new_version(self):
        try:
            assert self.find_clickable_element(PageLocators.locator_ptk_create_new_version)
            self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Клик на кнопку создания новой версии. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Клик на кнопку создания новой версии. Успешный.',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Клик на кнопку создания новой версии. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Клик на кнопку создания новой версии. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def click_excel_form(self):
        try:
            assert self.find_clickable_element(PageLocators.locator_ptk_click_excel_form)
            self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Клик на кнопку выгрузки эксель формы. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Клик на кнопку выгрузки эксель формы. Успешный.',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Клик на кнопку выгрузки эксель формы. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Клик на кнопку выгрузки эксель формы. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def searching_by_incorrect_ID(self):
        try:
            assert self.find_element(
                    PageLocators.locator_ptk_archive_form_20_searching_by_id).send_keys('randomid')
            self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Поиск проекта по некорректному идентификатору. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Поиск проекта по некорректному идентификатору. Успешный.',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Поиск проекта по некорректному идентификатору. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Поиск проекта по некорректному идентификатору. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def download_button(self):
        try:
            assert self.find_clickable_element(PageLocators.locator_ptk_download_button)
            self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Проверка кнопки загрузки. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Проверка кнопки загрузки. Успешный.',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Проверка кнопки загрузки. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Проверка кнопки загрузки. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def project_card_assert_deleted(self):
        try:
            assert self.find_element(PageLocators.locator_ptk_project_card_assert_deleted)
            self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Проверка удаления карты проекта. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Проверка удаления карты проекта. Успешный.',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Проверка удаления карты проекта. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Проверка удаления карты проекта. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def project_card_add_comment(self):
        try:
            assert self.find_element(PageLocators.locator_ptk_project_card_add_comment).send_keys(
                    'Тестовый комментарий' + time.ctime())
            self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Проверка добавления комментария в карточку проекта. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Проверка добавления комментария в карточку проекта. Успешный.',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Проверка добавления комментария в карточку проекта. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Проверка добавления комментария в карточку проекта. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def invisible_project_card(self):
        try:
            assert self.invisible_element(PageLocators.locator_ptk_get_task_name)
            self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Проверка того, что карточка не найдена. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Проверка того, что карточка не найдена. Успешный.',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Проверка того, что карточка не найдена. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Проверка того, что карточка не найдена. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def project_card_click_on_instructions(self):
        try:
            assert self.find_clickable_element(
                    PageLocators.locator_ptk_project_card_click_instructions)
            self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Клик на инструкцию в карточке проекта. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Клик на инструкцию в карточке проекта. Успешный.',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Клик на инструкцию в карточке проекта. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Клик на инструкцию в карточке проекта. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def project_card_find_instructions(self):
        try:
            assert self.find_element(PageLocators.locator_ptk_project_card_find_istructions)
            self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Поиск инструкции в карточке проекта. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Поиск инструкции в карточке проекта. Успешный.',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Поиск инструкции в карточке проекта. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Поиск инструкции в карточке проекта. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def go_to_test_calc(self):
        try:
            assert self.driver.get(PageLocators.locator_ptk_calculation_test_card)
            self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Переход в тестовый проект для калькуляции. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Переход в тестовый проект для калькуляции. Успешный.',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Переход в тестовый проект для калькуляции. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Переход в тестовый проект для калькуляции. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def project_card_fill_amount_4(self):
        try:
            assert self.find_element(
                    PageLocators.locator_ptk_project_card_fill_amount_4).send_keys('2516586')
            self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Заполнение продукта в четвертой строке. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Переход в тестовый проект для калькуляции. Успешный.',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Заполнение продукта в четвертой строке. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Заполнение продукта в четвертой строке. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def project_card_pick_table_4(self):
        try:
            assert self.find_element(PageLocators.locator_ptk_project_card_pick_table_4)
            find_table_4 = self.find_element(PageLocators.locator_ptk_project_card_pick_table_4)
            self.driver.execute_script("arguments[0].click()", find_table_4)
            if (self.find_element(PageLocators.locator_ptk_project_card_pick_table_4)):
                self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Выбор четвертой таблицы в карточке проекта. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Выбор четвертой таблицы в карточке проекта. Успешный.',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Выбор четвертой таблицы в карточке проекта. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Выбор четвертой таблицы в карточке проекта. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def project_card_pick_table_5(self):
        try:
            assert self.find_element(PageLocators.locator_ptk_project_card_pick_table_5)
            find_table_5 = self.find_element(PageLocators.locator_ptk_project_card_pick_table_5)
            self.driver.execute_script("arguments[0].click()", find_table_5)
            if (self.find_element(PageLocators.locator_ptk_project_card_pick_table_5)):
                self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Выбор пятой таблицы в карточке проекта. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Выбор четвертой таблицы в карточке проекта. Успешный.',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Выбор пятой таблицы в карточке проекта. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Выбор четвертой таблицы в карточке проекта. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def project_card_click_on_plus(self):
        try:
            assert self.find_clickable_element(PageLocators.locator_ptk_project_card_click_on_plus)
            self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Клик на плюс в карточке проекта. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Клик на плюс в карточке проекта. Успешный.',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Клик на плюс в карточке проекта. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Клик на плюс в карточке проекта. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def assert_invisible_plus(self):
        try:
            assert self.invisible_element(PageLocators.locator_ptk_project_card_click_on_plus)
            self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Проверка отсутствия плюса в карточке проекта. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Проверка отсутствия плюса в карточке проекта. Успешный.',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Проверка отсутствия плюса в карточке проекта. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Проверка отсутствия плюса в карточке проекта. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def project_card_pick_table_2(self):
        try:
            assert self.find_clickable_element(PageLocators.locator_ptk_project_card_pick_table_2)
            self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Выбор второй таблицы в карточке проекта. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Выбор второй таблицы в карточке проекта. Успешный.',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Выбор второй таблицы в карточке проекта. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Выбор второй таблицы в карточке проекта. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def project_card_fill_amount_5(self):
        try:
            assert self.find_element(
                    PageLocators.locator_ptk_project_card_fill_amount_5).send_keys('2516586')
            self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Заполнение количества продукта в пятой строке. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Заполнение количества продукта в пятой строке. Успешный.',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Заполнение количества продукта в пятой строке. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Заполнение количества продукта в пятой строке. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def project_card_fill_amount(self):
        try:
            assert self.find_element(PageLocators.locator_ptk_project_card_fill_amount).send_keys(
                    '2516586')
            self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Заполнение количества продукта в первой строке. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Заполнение количества продукта в первой строке. Успешный.',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Заполнение количества продукта в первой строке. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Заполнение количества продукта в первой строке. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def project_card_fill_amount_2(self):
        try:
            assert self.find_element(
                    PageLocators.locator_ptk_project_card_fill_amount_table_2).send_keys('2516586')
            self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Заполнение количества продукта во второй строке. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Заполнение количества продукта во второй строке. Успешный.',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Заполнение количества продукта во второй строке. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Заполнение количества продукта во второй строке. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def project_card_save_button(self):
        try:
            assert self.find_element(
                PageLocators.locator_ptk_project_card_save_button)
            save_button = self.find_element(
                PageLocators.locator_ptk_project_card_save_button)
            self.driver.execute_script("arguments[0].click()", save_button)

        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Клик на кнопку "Сохранить" в расчетной части. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Клик на кнопку "Сохранить" в расчетной части. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def project_card_assert_value_amount(self):
        try:
            assert self.find_element(
                    PageLocators.locator_ptk_project_card_assert_value_amount).text == ('2516586')
            self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Проверка введенного и отображенного значения в таблице рассчетов. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Проверка введенного и отображенного значения в таблице рассчетов. Успешный.',
                              attachment_type=AttachmentType.PNG)
                assert self.find_element(PageLocators.locator_ptk_project_card_delete_5)
                find_cross = self.find_element(PageLocators.locator_ptk_project_card_delete_5)
                self.driver.execute_script("arguments[0].click()", find_cross)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Проверка введенного и отображенного значения в таблице рассчетов. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Проверка введенного и отображенного значения в таблице рассчетов. Провален.',
                              attachment_type=AttachmentType.PNG)
            with allure.step('Удаление записи через крестик. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Удаление записи через крестик. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def project_card_assert_value_amount_5(self):
        try:
            assert self.find_element(
                    PageLocators.locator_ptk_project_card_assert_value_amount_5).text == ('2516586')
            self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step(
                    'Проверка введенного и отображенного значения в пятой строке таблицы рассчетов. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Проверка введенного и отображенного значения в пятой строке таблицы рассчетов. Успешный.',
                              attachment_type=AttachmentType.PNG)
            assert self.find_element(PageLocators.locator_ptk_project_card_delete_5)
            find_cross = self.find_element(PageLocators.locator_ptk_project_card_delete_5)
            self.driver.execute_script("arguments[0].click()", find_cross)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step(
                    'Проверка введенного и отображенного значения в пятой строке таблицы рассчетов. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Проверка введенного и отображенного значения в пятой строке таблицы рассчетов. Провален.',
                              attachment_type=AttachmentType.PNG)
            with allure.step('Удаление записи через крестик. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Удаление записи через крестик. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def project_card_assert_value_amount_4(self):
        try:
            assert self.find_element(
                    PageLocators.locator_ptk_project_card_assert_value_amount_4).text == ('2516586')
            self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step(
                    'Проверка введенного и отображенного значения в четвертой строке таблицы рассчетов. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Проверка введенного и отображенного значения в четвертой строке таблицы рассчетов. Успешный.',
                              attachment_type=AttachmentType.PNG)
            assert self.find_element(PageLocators.locator_ptk_project_card_delete_5)
            find_cross = self.find_element(PageLocators.locator_ptk_project_card_delete_5)
            self.driver.execute_script("arguments[0].click()", find_cross)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step(
                    'Проверка введенного и отображенного значения в четвертой строке таблицы рассчетов. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Проверка введенного и отображенного значения в четвертой строке таблицы рассчетов. Провален.',
                              attachment_type=AttachmentType.PNG)
            with allure.step('Удаление записи через крестик. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Удаление записи через крестик. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def project_card_assert_value_amount_2(self):
        try:
            assert self.find_element(
                    PageLocators.locator_ptk_project_card_assert_value_amount_2).text == ('2516586')
            self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step(
                    'Проверка введенного и отображенного значения во второй строке таблицы рассчетов. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Проверка введенного и отображенного значения во второй строке таблицы рассчетов. Успешный.',
                              attachment_type=AttachmentType.PNG)
            assert self.find_element(PageLocators.locator_ptk_project_card_delete_5)
            find_cross_2 = self.find_element(PageLocators.locator_ptk_project_card_delete_5)
            self.driver.execute_script("arguments[0].click()", find_cross_2)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step(
                    'Проверка введенного и отображенного значения во второй строке таблицы рассчетов. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Проверка введенного и отображенного значения во второй строке таблицы рассчетов. Провален.',
                              attachment_type=AttachmentType.PNG)
            with allure.step('Удаление записи через крестик. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Удаление записи через крестик. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def project_card_select_product(self):
        try:
            assert self.find_clickable_element(
                    PageLocators.locator_ptk_project_card_select_product)
            self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step(
                    'Выбор продукта в карточке продукта. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Выбор продукта в карточке продукта. Успешный.',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step(
                    'Выбор продукта в карточке продукта. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Выбор продукта в карточке продукта. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def project_card_select_table_1(self):
        try:
            assert self.find_clickable_element(PageLocators.locator_ptk_project_card_select_table_1)
            self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step(
                    'Выбор таблицы один в карточке проекта. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Выбор таблицы один в карточке проекта. Успешный.',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step(
                    'Выбор таблицы один в карточке проекта. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Выбор таблицы один в карточке проекта. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def project_card_select_b_1(self):
        try:
            assert self.find_clickable_element(PageLocators.locator_ptk_project_card_select_b_1)
            self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step(
                    'Выбор раздела Б1 в таблице карточки проекта. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Выбор раздела Б1 в таблице карточки проекта. Успешный.',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step(
                    'Выбор раздела Б1 в таблице карточки проекта. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Выбор раздела Б1 в таблице карточки проекта. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def project_card_pick_table_3(self):
        try:
            assert self.find_element(PageLocators.locator_ptk_project_card_pick_table_3)
            find_table_3 = self.find_element(PageLocators.locator_ptk_project_card_pick_table_3)
            self.driver.execute_script("arguments[0].click()", find_table_3)
            assert self.find_element(PageLocators.locator_ptk_project_card_pick_table_3)
            self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step(
                    'Выбор таблицы три в карточке проекта. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Выбор таблицы три в карточке проекта. Успешный.',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step(
                    'Выбор таблицы три в карточке проекта. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Выбор таблицы три в карточке проекта. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def project_card_fill_amount_3(self):
        try:
            assert self.find_element(
                    PageLocators.locator_ptk_project_card_fill_amount_table_3).send_keys('2516586')
            self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step(
                    'Заполнение количества продукта в строке три. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Заполнение количества продукта в строке три. Успешный.',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step(
                    'Заполнение количества продукта в строке три. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Заполнение количества продукта в строке три. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def project_card_assert_value_amount_3(self):
        try:
            assert self.find_element(
                    PageLocators.locator_ptk_project_card_assert_value_amount_3).text == ('2516586')
            self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step(
                    'Проверка введенного и отображенного значения в третьей строке таблицы рассчетов. Успешный'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Проверка введенного и отображенного значения в третьей строке таблицы рассчетов. Успешный',
                              attachment_type=AttachmentType.PNG)
            find_cross_3 = self.find_element(PageLocators.locator_ptk_project_card_delete_5)
            self.driver.execute_script("arguments[0].click()", find_cross_3)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step(
                    'Проверка введенного и отображенного значения в третьей строке таблицы рассчетов. Провален'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Проверка введенного и отображенного значения в третьей строке таблицы рассчетов. Провален',
                              attachment_type=AttachmentType.PNG)
            with allure.step('Удаление записи через крестик. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Удаление записи через крестик. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def assert_calculation(self):
        try:
            assert self.find_elements(PageLocators.locator_ptk_project_card_calculation_unc) and self.find_element(
                    PageLocators.locator_ptk_project_card_calculation_unc_i_nds) and self.find_element(
                PageLocators.locator_ptk_project_card_calculation_unc_prognoz)
            self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Поиск элементов рассчетной части. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Поиск элементов рассчетной части. Успешный.',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Поиск элементов рассчетной части. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Поиск элементов рассчетной части. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def cancel_calculation(self):
        try:
            assert self.find_clickable_element(PageLocators.locator_ptk_project_card_cancel_calculation)
            self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Выход из рассчетной части. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Выход из рассчетной части. Успешный.',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Выход из рассчетной части. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Выход из рассчетной части. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def assert_calculation_canceled(self):
        try:
            assert self.invisible_element(
                    PageLocators.locator_ptk_project_card_calculation_block_1)
            self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Проверка выхода из рассчетной части. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Проверка выхода из рассчетной части. Успешный.',
                              attachment_type=AttachmentType.PNG)
        except:
            with allure.step('Проверка выхода из рассчетной части. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Проверка выхода из рассчетной части. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def summa_s_nds(self):
        try:
            assert self.find_element(PageLocators.locator_ptk_project_card_edited_summa_s_nds).send_keys(Keys.CONTROL,
                                                                                                      "A",
                                                                                                      Keys.DELETE + '1234')
            self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Ввод значения в поле "Сумма с НДС". Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Ввод значения в поле "Сумма с НДС". Успешный.',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Ввод значения в поле "Сумма с НДС". Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Ввод значения в поле "Сумма с НДС". Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def giant_nds(self):
        try:
            assert self.find_element(PageLocators.locator_ptk_project_card_summa_s_nds).send_keys(Keys.CONTROL,
                                                                                               "A", Keys.DELETE)
            self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            assert self.find_element(PageLocators.locator_ptk_project_card_summa_s_nds).send_keys(
                    '1234567890123')
            self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            assert self.find_clickable_element(PageLocators.locator_ptk_project_card_cancel_calculation)
            self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            assert self.find_element(
                    PageLocators.locator_ptk_project_card_info_block).text == '1234567890123.00'
            self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Ввод двенадцати значного значения в поле "Сумма с НДС". Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Ввод двенадцати значного значения в поле "Сумма с НДС". Успешный.',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Ввод двенадцати значного значения в поле "Сумма с НДС". Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Ввод двенадцати значного значения в поле "Сумма с НДС". Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def assert_summa_s_nds(self):
        try:
            assert self.find_element(PageLocators.locator_ptk_assert_summa_s_nds).text == (
                    '1 234')
            self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Проверка отображаемого значения в поле "Сумма с НДС". Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Проверка отображаемого значения в поле "Сумма с НДС". Успешный.',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Проверка отображаемого значения в поле "Сумма с НДС". Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Проверка отображаемого значения в поле "Сумма с НДС". Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def click_on_calculation(self):
        try:
            assert self.find_clickable_element(
                    PageLocators.locator_ptk_project_card_click_on_calculation)
            self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Клик на кнопку "Рассчитать". Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Проверка отображаемого значения в поле "Сумма с НДС". Успешный.',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Клик на кнопку "Рассчитать". Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Клик на кнопку "Рассчитать". Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def project_card_select_altay_region(self):
        try:
            assert self.find_clickable_element(
                    PageLocators.locator_ptk_project_card_select_altay_region)
            self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Выбор региона Алтай в карточке проекта. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Выбор региона Алтай в карточке проекта. Успешный.',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Выбор региона Алтай в карточке проекта. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Выбор региона Алтай в карточке проекта. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def project_card_select_amur_region(self):
        try:
            assert self.find_clickable_element(
                    PageLocators.locator_ptk_project_card_select_amur_region)
            self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Выбор региона Амур в карточке проекта. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Выбор региона Амур в карточке проекта. Успешный.',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Выбор региона Амур в карточке проекта. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Выбор региона Амур в карточке проекта. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def project_card_assert_altay_selected(self):
        try:
            assert self.find_element(
                    PageLocators.locator_ptk_project_card_check_altay_selected)
            self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Проверка отображения региона Алтай после выбора в карточке проекта. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Проверка отображения региона Алтай после выбора в карточке проекта. Успешный.',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Проверка отображения региона Алтай после выбора в карточке проекта. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Проверка отображения региона Алтай после выбора в карточке проекта. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def project_card_assert_amur_selected(self):
        try:
            assert self.find_element(
                    PageLocators.locator_ptk_project_card_check_amur_selected)
            self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Проверка отображения региона Амур после выбора в карточке проекта. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Проверка отображения региона Амур после выбора в карточке проекта. Успешный.',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Проверка отображения региона Амур после выбора в карточке проекта. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Проверка отображения региона Амур после выбора в карточке проекта. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def project_card_click_on_body(self):
        try:
            assert self.find_clickable_element(
                    PageLocators.locator_ptk_project_card_click_on_body)
            self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Клик на тело страницы карточки проекта. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Клик на тело страницы карточки проекта. Успешный.',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Клик на тело страницы карточки проекта. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Клик на тело страницы карточки проекта. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def project_card_commentary(self):
        try:
            assert self.find_element(PageLocators.locator_ptk_project_card_commentary)
            project_card_commentary = self.find_element(PageLocators.locator_ptk_project_card_commentary)
            assert self.find_element(PageLocators.locator_ptk_project_card_commentary)
            self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Поиск поля для комментария в карточке страницы. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Поиск поля для комментария в карточке страницы. Успешный.',
                              attachment_type=AttachmentType.PNG)
            assert project_card_commentary.send_keys(
                    'Тестовый комментарий' + time.ctime())
            self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Ввод комментария с текущей датой в карточке страницы. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Поиск поля для комментария в карточке страницы. Успешный.',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Поиск поля для комментария в карточке страницы. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Поиск поля для комментария в карточке страницы. Провален.',
                              attachment_type=AttachmentType.PNG)
            with allure.step('Ввод комментария с текущей датой в карточке страницы. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Поиск поля для комментария в карточке страницы. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def get_comment_value(self):
        try:
            global get_comment_value
            get_comment_value = self.find_element(PageLocators.locator_ptk_comment).get_attribute('value')
            if (self.find_element(PageLocators.locator_ptk_comment).get_attribute('value')):
                self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Забираем значение комментария. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Забираем значение комментария. Успешный.',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Забираем значение комментария. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Забираем значение комментария. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def find_equal_commentary(self):
        try:
            assert self.find_element(PageLocators.locator_ptk_comment).get_attribute('value')
            self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Находим искомый комментарий. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Находим искомый комментарий. Успешный.',
                              attachment_type=AttachmentType.PNG)
            assert self.find_element(PageLocators.locator_ptk_comment).get_attribute('value')
            get_value = self.find_element(PageLocators.locator_ptk_comment).get_attribute('value')
            assert get_comment_value == get_value
            assert self.find_element(PageLocators.locator_ptk_comment).send_keys(
                    Keys.CONTROL + 'A' + Keys.DELETE)
            self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Сравниваем значения искомого комментария с найденным. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Сравниваем значения искомого комментария с найденным. Успешный.',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Находим искомый комментарий. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Находим искомый комментарий. Провален.',
                              attachment_type=AttachmentType.PNG)
            with allure.step('Сравниваем значения искомого комментария с найденным. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Сравниваем значения искомого комментария с найденным. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def first_click_download_xlsx_project_card(self):
        try:
            assert self.find_clickable_element(
                    PageLocators.locator_ptk_first_click_download_xlsx_project_card)
            self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Выбираем эксель выгрузку на карточке проекта. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Выбираем эксель выгрузку на карточке проекта. Успешный.',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Выбираем эксель выгрузку на карточке проекта. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Выбираем эксель выгрузку на карточке проекта. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def click_download_xlsx_project_card(self):
        try:
            assert self.find_clickable_element(
                    PageLocators.locator_ptk_click_download_xlsx_project_card)
            self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Загружаем эксель выгрузку на карточке проекта. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Загружаем эксель выгрузку на карточке проекта. Успешный.',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Загружаем эксель выгрузку на карточке проекта. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Загружаем эксель выгрузку на карточке проекта. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def fill_admin_login(self):
            assert self.find_element(PageLocators.locator_ptk_fill_login).send_keys('user1@unc.ru')
            self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Заполняем логин администратора. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Заполняем логин администратора. Успешный.',
                              attachment_type=AttachmentType.PNG)
            if AssertionError:
                self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
                with allure.step('Переход на страницу логина. Провален.'):
                    allure.attach(self.driver.get_screenshot_as_png(), name='Переход на страницу логина. Провален.',
                                attachment_type=AttachmentType.PNG)
                logging.exception('')
                logging.critical('TEST FAILED')

    def fill_free_login(self, login):
        try:
            assert self.find_element(PageLocators.locator_ptk_fill_login).send_keys(login)
            self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Заполняем логин произвольным значением. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Заполняем логин произвольным значением. Успешный.',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Заполняем логин произвольным значением. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Заполняем логин произвольным значением. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def fill_free_password(self, password):
        try:
            assert self.find_element(PageLocators.locator_ptk_fill_login).send_keys(password)
            self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Заполняем пароль произвольным значением. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Заполняем пароль произвольным значением. Успешный.',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Заполняем пароль произвольным значением. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Заполняем пароль произвольным значением. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def find_login_button(self):
        try:
            assert self.find_element(PageLocators.locator_ptk_login_button)
            self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Поиск кнопки логина. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Поиск кнопки логина. Успешный.',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Поиск кнопки логина. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Поиск кнопки логина. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def fill_admin_password(self):
            assert self.find_element(PageLocators.locator_ptk_fill_password).send_keys('user1@unc.ru')
            self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Заполнение пароля администратора. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Заполнение пароля администратора. Успешный.',
                              attachment_type=AttachmentType.PNG)
            if AssertionError:
                self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
                with allure.step('Заполнение пароля администратора. Провален.'):
                    allure.attach(self.driver.get_screenshot_as_png(),
                                name='Заполнение пароля администратора. Провален.',
                                attachment_type=AttachmentType.PNG)
                logging.exception('')
                logging.critical('TEST FAILED')

    def click_login_button(self):
            assert self.find_clickable_element(PageLocators.locator_ptk_login_button)
            self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Клик на кнопку логина. Успешный.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Клик на кнопку логина. Успешный.',
                              attachment_type=AttachmentType.PNG)
            if AssertionError:
                self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
                with allure.step('Клик на кнопку логина. Провален.'):
                    allure.attach(self.driver.get_screenshot_as_png(),
                                name='Клик на кнопку логина. Провален.',
                                attachment_type=AttachmentType.PNG)
                logging.exception('')
                logging.critical('TEST FAILED')

    def authorization_assert(self):
        try:
            assert self.find_element(PageLocators.locator_ptk_authorization_assert)
            self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Authorization granted"}}')
            with allure.step('Проверка авторизации. Успешный'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Проверка авторизации. Успешный',
                              attachment_type=AttachmentType.PNG)
        except:
            assert False
        else:
            pass


    def project_description_text(self):
        try:
            assert self.find_element(PageLocators.locator_ptk_project_description_text)
            self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Поиск поля с описанием проекта. Успешный'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Поиск поля с описанием проекта. Успешный',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Поиск поля с описанием проекта. Провален'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Поиск поля с описанием проекта. Провален',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def project_description_text_part_2(self):
        try:
            assert self.find_element(
                    PageLocators.locator_ptk_project_description_text_part_2)
            self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Поиск поля с описанием проекта, продолжение. Успешный'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Поиск поля с описанием проекта, продолжение. Успешный',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Поиск поля с описанием проекта, продолжение. Провален'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Поиск поля с описанием проекта, продолжение. Провален',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def find_project_by_3_last_numbers(self):
        try:
            global last_3_numbers
            assert self.find_element(
                PageLocators.locator_ptk_find_project_by_3_last_numbers)
            find_project_by_3_last_numbers = self.find_element(
                PageLocators.locator_ptk_find_project_by_3_last_numbers).get_attribute('innerText')
            assert self.find_element(
                    PageLocators.locator_ptk_find_project_by_3_last_numbers).get_attribute('innerText')
            self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Поиск проекта по трем последним цифрам. Успешный'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Поиск проекта по трем последним цифрам. Успешный',
                              attachment_type=AttachmentType.PNG)
            last_3_numbers = (str(find_project_by_3_last_numbers)[-3:])
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Поиск проекта по трем последним цифрам. Провален'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Поиск проекта по трем последним цифрам. Провален',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def show_button(self):
        try:
            assert self.find_clickable_element(PageLocators.locator_ptk_project_card_show_button)
            self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Проверка кнопки "Показать". Успешный'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Проверка кнопки "Показать". Успешный',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Проверка кнопки "Показать". Провален'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Проверка кнопки "Показать". Провален',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def searching_field(self):
        try:
            assert self.find_element(PageLocators.locator_ptk_searching_field).send_keys(last_3_numbers)
            self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Ввод в поисковое поле трех последних цифр проекта. Успешный'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Ввод в поисковое поле трех последних цифр проекта. Успешный',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Ввод в поисковое поле трех последних цифр проекта. Провален'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Ввод в поисковое поле трех последних цифр проекта. Провален',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def fill_searching_field(self, item):
        try:
            assert self.find_element(PageLocators.locator_ptk_searching_field).send_keys(item)
            self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Ввод в поисковое поле произвольного значения. Успешный'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Ввод в поисковое поле трех последних цифр проекта. Успешный',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Ввод в поисковое поле произвольного значения. Провален'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Ввод в поисковое поле трех последних цифр проекта. Провален',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def find_projects_by_3_last_numbers(self):
        try:
            assert self.find_element(
                    PageLocators.locator_ptk_find_project_by_3_last_numbers).text == last_3_numbers
            self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Проект по трем последним числам найден. Успешный'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Проект по трем последним числам найден. Успешный',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Проект по трем последним числам найден. Провален'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Проект по трем последним числам найден. Провален',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def find_project_by_4_last_numbers(self):
        try:
            global last_4_numbers
            assert self.find_element(
                PageLocators.locator_ptk_find_project_by_3_last_numbers)
            find_project_by_3_last_numbers = self.find_element(
                PageLocators.locator_ptk_find_project_by_3_last_numbers).get_attribute('innerText')
            assert self.find_element(
                    PageLocators.locator_ptk_find_project_by_3_last_numbers).get_attribute('innerText')
            self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Поиск проекта по последним четырем цифрам. Успешный'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Поиск проекта по последним четырем цифрам. Успешный',
                              attachment_type=AttachmentType.PNG)
            last_4_numbers = (str(find_project_by_3_last_numbers)[-4:])
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Поиск проекта по последним четырем цифрам. Провален'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Поиск проекта по последним четырем цифрам. Провален',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def searching_field_4_symbols(self):
        try:
            assert self.find_element(PageLocators.locator_ptk_searching_field).send_keys(last_4_numbers)
            self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Ввод в поисковое поле последние четыре цифры. Успешный'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Ввод в поисковое поле последние четыре цифры. Успешный',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Ввод в поисковое поле последние четыре цифры. Провален'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Ввод в поисковое поле последние четыре цифры. Провален',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def find_projects_by_4_last_numbers(self):
        try:
            assert self.find_element(
                    PageLocators.locator_ptk_find_project_by_3_last_numbers).text == last_4_numbers
            self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Поиск проекта по последним четырем цифрам. Успешный'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Поиск проекта по последним четырем цифрам. Успешный',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Поиск проекта по последним четырем цифрам. Провален'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Поиск проекта по последним четырем цифрам. Провален',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def find_full_project_id(self):
        try:
            assert self.find_element(
                PageLocators.locator_ptk_find_project_by_3_last_numbers).get_attribute('innerText')
            global find_full_project_id
            find_full_project_id = self.find_element(
                PageLocators.locator_ptk_find_project_by_3_last_numbers).get_attribute('innerText')
            assert self.find_element(
                    PageLocators.locator_ptk_find_project_by_3_last_numbers).get_attribute('innerText')
            self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Получение полного значения идентификатора проекта. Успешный'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Получение полного значения идентификатора проекта. Успешный',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Получение полного значения идентификатора проекта. Провален'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Получение полного значения идентификатора проекта. Провален',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def searching_field_full_id(self):
        try:
            assert self.find_element(PageLocators.locator_ptk_searching_field).send_keys(find_full_project_id)
            self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Поиск полного значения идентификатора проекта. Успешный'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Получение полного значения идентификатора проекта. Успешный',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Поиск полного значения идентификатора проекта. Провален'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Получение полного значения идентификатора проекта. Провален',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def searching_field_full_id_and_space_at_the_end(self):
        try:
            assert self.find_element(PageLocators.locator_ptk_searching_field).send_keys(find_full_project_id,
                                                                                          Keys.SPACE)
            self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Поиск полного значения идентификатора с пробелом в конце. Успешный'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Поиск полного значения идентификатора с пробелом в конце. Успешный',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Поиск полного значения идентификатора с пробелом в конце. Провален'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Поиск полного значения идентификатора с пробелом в конце. Провален',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def searching_field_full_id_and_space_at_the_start(self):
        try:
            assert self.find_element(PageLocators.locator_ptk_searching_field).send_keys(Keys.SPACE,
                                                                                          find_full_project_id)
            self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Поиск полного значения идентификатора с пробелом в начале. Успешный'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Поиск полного значения идентификатора с пробелом в начале. Успешный',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Поиск полного значения идентификатора с пробелом в начале. Провален'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Поиск полного значения идентификатора с пробелом в начале. Провален',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def double_id(self):
        try:
            assert self.find_element(PageLocators.locator_ptk_searching_field).send_keys(find_full_project_id, Keys.SPACE,
                                                                                      find_full_project_id)
            self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Поиск полного значения идентификатора дублированный. Успешный'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Поиск полного значения идентификатора дублированный. Успешный',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Поиск полного значения идентификатора дублированный. Провален'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Поиск полного значения идентификатора дублированный. Провален',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def searching_field_full_id_and_double_space_at_the_end(self):
        try:
            assert self.find_element(PageLocators.locator_ptk_searching_field).send_keys(find_full_project_id, Keys.SPACE,
                                                                                      Keys.SPACE)
            self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Поиск полного значения идентификатора с двойным пробелом в конце. Успешный'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Поиск полного значения идентификатора с двойным пробелом в конце. Успешный',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Поиск полного значения идентификатора с двойным пробелом в конце. Провален'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Поиск полного значения идентификатора с двойным пробелом в конце. Провален',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def assert_project_full_id(self):
        try:
            assert self.find_element(
                    PageLocators.locator_ptk_find_project_by_3_last_numbers).text == find_full_project_id
            self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Проверка того, что проект найден по полному идентификатору. Успешный'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Проверка того, что проект найден по полному идентификатору. Успешный',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Проверка того, что проект найден по полному идентификатору. Провален'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Проверка того, что проект найден по полному идентификатору. Провален',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def get_task_name(self):
        try:
            assert self.find_element(PageLocators.locator_ptk_get_task_name).get_attribute('innerText')
            global get_task_name
            get_task_name = self.find_element(PageLocators.locator_ptk_get_task_name).get_attribute('innerText')
            if (self.find_element(PageLocators.locator_ptk_get_task_name).get_attribute('innerText')):
                self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Получение названия проекта. Успешный'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Получение названия проекта. Успешный',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Получение названия проекта. Провален'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Получение названия проекта. Провален',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def searching_field_task_name(self):
        try:
            assert self.find_element(PageLocators.locator_ptk_searching_field).send_keys(get_task_name)
            self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Ввод названия проекта в поисковое поле. Успешный'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Ввод названия проекта в поисковое поле. Успешный',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Ввод названия проекта в поисковое поле. Провален'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Ввод названия проекта в поисковое поле. Провален',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def searching_field_custom_searching(self, text):
        try:
            assert self.find_element(PageLocators.locator_ptk_searching_field).send_keys(text)
            self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Ввод произвольного значения в поле поиска. Успешный'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Ввод произвольного значения в поле поиска. Успешный',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Ввод произвольного значения в поле поиска. Провален'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Ввод произвольного значения в поле поиска. Провален',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def assert_part_text(self):
        try:
            assert self.find_element(
                    PageLocators.locator_ptk_get_task_name).text == 'Техперевооружение ПС 500 кВ Магистральная, ПС 220 кВ Новая, ПС 220 кВ Восточно-Моховая, ПС 220 кВ Картопья, ПС 500 кВ Пыть-Ях, ПС 220 кВ Полоцкая в части модернизации 13-ти выключателей с заменой маслонаполненных вводов на вводы с твердой изоляцией (77 высоковольтных ввода), в конце добавляем рандомный текст '
            self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Поиск части текста в проекте. Успешный'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Поиск части текста в проекте. Успешный',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Поиск части текста в проекте. Провален'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Поиск части текста в проекте. Провален',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def assert_project_task_name(self):
        try:
            assert self.find_element(PageLocators.locator_ptk_get_task_name).text == get_task_name
            self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Проверка соответствия текста найденной задачи с искомой. Успешный'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Проверка соответствия текста найденной задачи с искомой. Успешный',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Проверка соответствия текста найденной задачи с искомой. Провален'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Проверка соответствия текста найденной задачи с искомой. Провален',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def assert_project_by_1_word(self):
        try:
            assert self.find_element(PageLocators.locator_ptk_get_task_name).text == 'Заменой'
            self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Поиск проекта по одному слову. Успешный'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Поиск проекта по одному слову. Успешный',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Поиск проекта по одному слову. Провален'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Поиск проекта по одному слову. Провален',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def get_project_filial(self):
        try:
            global get_project_filial
            assert self.find_element(PageLocators.locator_ptk_get_project_filial)
            get_project_filial = self.find_element(PageLocators.locator_ptk_get_project_filial).get_attribute(
                'innerText')
            assert self.find_element(PageLocators.locator_ptk_get_project_filial).get_attribute(
                    'innerText')
            self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Получение значения филиала проекта. Успешный'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Получение значения филиала проекта. Успешный',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Получение значения филиала проекта. Провален'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Получение значения филиала проекта. Провален',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def searching_field_project_filial(self):
        try:
            assert self.find_element(PageLocators.locator_ptk_searching_field).send_keys(get_project_filial)
            self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Ввод филиала проекта в поисковое поле. Успешный'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Ввод филиала проекта в поисковое поле. Успешный',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Ввод филиала проекта в поисковое поле. Провален'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Ввод филиала проекта в поисковое поле. Провален',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def searching_space_at_the_start_field_project_filial(self):
        try:
            assert self.find_element(PageLocators.locator_ptk_searching_field).send_keys(Keys.SPACE, get_project_filial)
            self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Поиск проекта с пробелом в начале. Успешный'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Поиск проекта с пробелом в начале. Успешный',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Ввод филиала проекта в поисковое поле. Провален'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Ввод филиала проекта в поисковое поле. Провален',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def searching_space_at_the_end_field_project_filial(self):
        try:
            assert self.find_element(PageLocators.locator_ptk_searching_field).send_keys(get_project_filial, Keys.SPACE)
            self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Поиск проекта с пробелом в конце. Успешный'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Поиск проекта с пробелом в конце. Успешный',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Поиск проекта с пробелом в конце. Провален'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Поиск проекта с пробелом в конце. Провален',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def searching_filial_with_incorrect_filling(self):
        try:
            assert self.find_element(
                    PageLocators.locator_ptk_searching_field).send_keys('МЭС Урала рандомное заполнение')
            self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Поиск проекта по филиалу - некорректное заполнение. Успешный'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Поиск проекта по филиалу - некорректное заполнение. Успешный',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Поиск проекта по филиалу - некорректное заполнение. Провален'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Поиск проекта по филиалу - некорректное заполнение. Провален',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def assert_project_filial(self):
        try:
            assert self.find_element(
                    PageLocators.locator_ptk_get_project_filial).text == get_project_filial
            self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Сравнение филиалов найденного проекта с искомым. Успешный'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Сравнение филиалов найденного проекта с искомым. Успешный',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Сравнение филиалов найденного проекта с искомым. Провален'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Сравнение филиалов найденного проекта с искомым. Провален',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def get_task_name_10_symbols(self):
        try:
            assert self.find_element(PageLocators.locator_ptk_get_task_name).get_attribute(
                'innerText')
            global last_10_symbols
            get_task_name_10_symbols = self.find_element(PageLocators.locator_ptk_get_task_name).get_attribute(
                'innerText')
            last_10_symbols = (str(get_task_name_10_symbols)[:10])
            assert self.find_element(PageLocators.locator_ptk_get_task_name).get_attribute(
                    'innerText')
            self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            assert (str(get_task_name_10_symbols)[:10])
            self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Получение десяти символов проекта. Успешный'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Получение десяти символов проекта. Успешный',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Получение десяти символов проекта. Провален'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Получение десяти символов проекта. Провален',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def searching_field_task_name_10_symbols(self):
        try:
            assert self.find_element(PageLocators.locator_ptk_searching_field).send_keys(last_10_symbols)
            self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Ввод десяти символов проекта в поисковое поле. Успешный'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Ввод десяти символов проекта в поисковое поле. Успешный',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Ввод десяти символов проекта в поисковое поле. Провален'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Ввод десяти символов проекта в поисковое поле. Провален',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def assert_task_name_10_symbols(self):
        try:
            assert self.find_element(
                    PageLocators.locator_ptk_get_task_name).text == last_10_symbols
            self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Сравнение найденного проекта с искомым. Успешный'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Сравнение найденного проекта с искомым. Успешный',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Сравнение найденного проекта с искомым. Провален'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Сравнение найденного проекта с искомым. Провален',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def get_task_name_20_symbols(self):
        global last_20_symbols
        assert self.find_element(PageLocators.locator_ptk_get_task_name)
        get_task_name_20_symbols = self.find_element(PageLocators.locator_ptk_get_task_name).get_attribute(
            'innerText')
        last_20_symbols = (str(get_task_name_20_symbols)[:20])

    def searching_field_task_name_20_symbols(self):
        try:
            assert self.find_element(PageLocators.locator_ptk_searching_field).send_keys(last_20_symbols)
            self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "List of projects found"}}')
            with allure.step('Поиск проекта по двадцати символам. Успешный'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Поиск проекта по двадцати символам. Успешный',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "List of projects not found"}}')
            with allure.step('Поиск проекта по двадцати символам. Провален'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Поиск проекта по двадцати символам. Провален',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def assert_task_name_20_symbols(self):
        try:
            assert self.find_element(
                    PageLocators.locator_ptk_get_task_name).text == last_20_symbols
            self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "List of projects found"}}')
            with allure.step('Сравнение найденного проекта с искомым по двадцати символам. Успешный'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Сравнение найденного проекта с искомым по двадцати символам. Успешный',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "List of projects not found"}}')
            with allure.step('Сравнение найденного проекта с искомым по двадцати символам. Провален'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Сравнение найденного проекта с искомым по двадцати символам. Провален',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def find_list_of_projects(self):
        try:
            assert self.find_elements(PageLocators.locator_ptk_list_of_all_projects)
            self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "List of projects found"}}')
            with allure.step('Поиск списка проектов. Успешный'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Поиск списка проектов. Успешный',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "List of projects not found"}}')
            with allure.step('Поиск списка проектов. Провален'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Поиск списка проектов. Провален',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def main_page_click_settings_button(self):
        try:
            assert self.find_clickable_element(
                    PageLocators.locator_ptk_settings_button_main_page)
            self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "List of projects found"}}')
            with allure.step('Клик на кнопку настроек на главной странице. Успешный'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Клик на кнопку настроек на главной странице. Успешный',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "List of projects not found"}}')
            with allure.step('Клик на кнопку настроек на главной странице. Успешный'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Клик на кнопку настроек на главной странице. Успешный',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def assert_table_of_investments(self):
        try:
            assert self.find_element(PageLocators.locator_ptk_table_invesment_programms) and self.find_element(PageLocators.locator_ptk_investment_parameters) and self.find_element(PageLocators.locator_pkt_investment_index_def)
            self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "List of projects found"}}')
            with allure.step('Поиск инструментов инвестирования. Успешный'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Поиск инструментов инвестирования. Успешный',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "List of projects not found"}}')
            with allure.step('Поиск инструментов инвестирования. Провален'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Поиск инструментов инвестирования. Провален',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def click_version_selector(self):
        try:
            assert self.find_clickable_element(PageLocators.locator_ptk_click_version_selector)
            self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "List of projects found"}}')
            with allure.step('Клик на селектор версий. Успешный'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Клик на селектор версий. Успешный',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "List of projects not found"}}')
            with allure.step('Клик на селектор версий. Провален'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Клик на селектор версий. Провален',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def select_sd_version(self):
        try:
            assert self.find_clickable_element(PageLocators.locator_ptk_select_SD_version)
            self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "List of projects found"}}')
            with allure.step('Выбор СД версии. Успешный'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Выбор СД версии. Успешный',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "List of projects not found"}}')
            with allure.step('Выбор СД версии. Провален'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Выбор СД версии. Провален',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def assert_SD_version_selected(self):
        try:
            assert self.find_element(
                    PageLocators.locator_ptk_assert_SD_version_selected).text == 'Версия для СД'
            self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "List of projects found"}}')
            with allure.step('Проверка того, что выбрана СД версия. Успешный'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Проверка того, что выбрана СД версия. Успешный',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "List of projects not found"}}')
            with allure.step('Проверка того, что выбрана СД версия. Провален'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Проверка того, что выбрана СД версия. Провален',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def get_project_id_new_version(self):
        global project_id_new_version
        assert self.find_element(
            PageLocators.locator_ptk_find_project_by_3_last_numbers)
        project_id_new_version = self.find_element(
            PageLocators.locator_ptk_find_project_by_3_last_numbers).get_attribute('innerText')

    def assert_projects_id_not_equal(self):
        try:
            assert self.find_element(
                    PageLocators.locator_ptk_find_project_by_3_last_numbers).text != project_id_new_version
            self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Проверка того, идентификаторы проектов не одинаковы. Успешный'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Проверка того, что выбрана СД версия. Успешный',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Проверка того, идентификаторы проектов не одинаковы. Провален'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Проверка того, что выбрана СД версия. Провален',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def click_on_body_of_main_page(self):
        try:
            assert self.find_clickable_element(PageLocators.locator_ptk_body_of_main_page)
            self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Клик на тело главной страницы. Успешный'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Клик на тело главной страницы. Успешный',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Клик на тело главной страницы. Провален'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Клик на тело главной страницы. Провален',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def get_project_id(self):
        global get_project_id
        assert self.find_element(PageLocators.locator_ptk_find_project_by_3_last_numbers)
        get_project_id = self.find_element(PageLocators.locator_ptk_find_project_by_3_last_numbers).get_attribute(
            'innerText')

    def go_to_project_card(self):
        try:
            assert self.find_clickable_element(PageLocators.locator_ptk_find_project_by_3_last_numbers)
            self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Passed"}}')
            with allure.step('Переход на карту проекта. Успешный'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Переход на карту проекта. Успешный',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Failed"}}')
            with allure.step('Переход на карту проекта. Провален'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Переход на карту проекта. Провален',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def assert_project_id(self):
        try:
            assert self.find_element(PageLocators.locator_ptk_assert_project_id_in_card).text == get_project_id
            self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "ID equals"}}')
            with allure.step('Проверка идентичности идентификаторов. Успешный'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Проверка идентичности идентификаторов. Успешный',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "ID not equal"}}')
            with allure.step('Проверка идентичности идентификаторов. Провален'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Проверка идентичности идентификаторов. Провален',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')

    def scroll_page_down(self):
        assert self.find_element(PageLocators.locator_ptk_skeleton_of_main_page)
        scroll_page_down = self.find_element(PageLocators.locator_ptk_skeleton_of_main_page).send_keys(
            Keys.PAGE_DOWN)
        scroll_page_down = self.find_element(PageLocators.locator_ptk_skeleton_of_main_page).send_keys(
            Keys.PAGE_DOWN)
        scroll_page_down = self.find_element(PageLocators.locator_ptk_skeleton_of_main_page).send_keys(
            Keys.PAGE_DOWN)
        scroll_page_down = self.find_element(PageLocators.locator_ptk_skeleton_of_main_page).send_keys(
            Keys.PAGE_DOWN)
        scroll_page_down = self.find_element(PageLocators.locator_ptk_skeleton_of_main_page).send_keys(
            Keys.PAGE_DOWN)
        scroll_page_down = self.find_element(PageLocators.locator_ptk_skeleton_of_main_page).send_keys(
            Keys.PAGE_DOWN)
        scroll_page_down = self.find_element(PageLocators.locator_ptk_skeleton_of_main_page).send_keys(
            Keys.PAGE_DOWN)
        scroll_page_down = self.find_element(PageLocators.locator_ptk_skeleton_of_main_page).send_keys(
            Keys.PAGE_DOWN)
        scroll_page_down = self.find_element(PageLocators.locator_ptk_skeleton_of_main_page).send_keys(
            Keys.PAGE_DOWN)
        scroll_page_down = self.find_element(PageLocators.locator_ptk_skeleton_of_main_page).send_keys(
            Keys.PAGE_DOWN)
        scroll_page_down = self.find_element(PageLocators.locator_ptk_skeleton_of_main_page).send_keys(
            Keys.PAGE_DOWN)
        scroll_page_down = self.find_element(PageLocators.locator_ptk_skeleton_of_main_page).send_keys(
            Keys.PAGE_DOWN)
        scroll_page_down = self.find_element(PageLocators.locator_ptk_skeleton_of_main_page).send_keys(
            Keys.PAGE_DOWN)
        scroll_page_down = self.find_element(PageLocators.locator_ptk_skeleton_of_main_page).send_keys(
            Keys.PAGE_DOWN)
        scroll_page_down = self.find_element(PageLocators.locator_ptk_skeleton_of_main_page).send_keys(
            Keys.PAGE_DOWN)
        scroll_page_down = self.find_element(PageLocators.locator_ptk_skeleton_of_main_page).send_keys(
            Keys.PAGE_DOWN)

    def click_on_page_upper(self):
        try:
            assert self.find_clickable_element(PageLocators.locator_ptk_click_on_page_upper)
            self.driver.execute_script(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Page upper clicked"}}')
            with allure.step('Клик на кнопку подъема страницы. Успешный'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Клик на кнопку подъема страницы. Успешный',
                              attachment_type=AttachmentType.PNG)
        except:
            self.driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Page upper not clicked"}}')
            with allure.step('Клик на кнопку подъема страницы. Провален'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Клик на кнопку подъема страницы. Провален',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
